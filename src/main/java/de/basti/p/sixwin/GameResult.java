package de.basti.p.sixwin;

import java.util.HashMap;
import java.util.Map;

public class GameResult {

	private final Map<IPlayerStrategy, Double> strategyToPointsMap;
	private final int count;

	public GameResult(Map<IPlayerStrategy, Double> strategyToPointsMap, int count) {
		super();
		this.strategyToPointsMap = strategyToPointsMap;
		this.count = count;
	}

	public GameResult(Map<IPlayerStrategy, Double> strategyToPointsMap) {
		super();
		this.count = 1;
		this.strategyToPointsMap = strategyToPointsMap;
	}

	public Map<IPlayerStrategy, Double> getStrategyToPointsMap() {
		return strategyToPointsMap;
	}

	public GameResult merge(GameResult result) {
		if (count == 0) {
			return result;
		}
		Map<IPlayerStrategy, Double> strategyToPointsMap = new HashMap<IPlayerStrategy, Double>();
		for (IPlayerStrategy str : this.strategyToPointsMap.keySet()) {
			double currentValue = this.strategyToPointsMap.get(str);
			double newValue = result.getStrategyToPointsMap().get(str);
			double newWeigtetValue = (currentValue * count + newValue * result.count) / (count + result.count);
			strategyToPointsMap.put(str, newWeigtetValue);
		}
		return new GameResult(strategyToPointsMap, count + result.count);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		strategyToPointsMap.forEach((s, i) -> {
			builder.append(s.getName());
			builder.append(":");
			builder.append(i);
			builder.append("\n");
		});
		return builder.toString();
	}

}
