package de.basti.p.sixwin;

import java.util.Set;

public class Player {

	private Set<Card> cards;
	private IPlayerStrategy strategy;
	private int points = 0;

	public Player(Set<Card> cards, IPlayerStrategy strategy) {
		super();
		this.cards = cards;
		this.strategy = strategy;
	}

	public Set<Card> getCards() {
		return cards;
	}

	public IPlayerStrategy getStrategy() {
		return strategy;
	}

	public double getPoints() {
		return points;
	}

	public void addPoint(int p) {
		points += p;
	}
}
