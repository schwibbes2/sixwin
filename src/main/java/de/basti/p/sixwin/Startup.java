package de.basti.p.sixwin;

import java.util.List;

import com.google.common.collect.Lists;

import de.basti.p.sixwin.strategy.RandomStrategy;
import de.basti.p.sixwin.strategy.SimpleStrategy;

public class Startup {

	public static void main(String[] args) {


		List<IPlayerStrategy> strategies = Lists.newArrayList(new RandomStrategy(), new RandomStrategy(),
				new SimpleStrategy(), new SimpleStrategy());
		MassGameManager gameManager = new MassGameManager(strategies, 50_000);
		GameResult result = gameManager.runGames();
		System.out.println(result);
	}

}
