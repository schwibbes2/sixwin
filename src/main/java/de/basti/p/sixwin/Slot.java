package de.basti.p.sixwin;

import java.util.Collections;
import java.util.List;
import java.util.UUID;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;

public class Slot {

	private List<Card> cards;
	private UUID id;

	protected Slot(Card card) {
		this(Lists.newArrayList(card));
	}

	private Slot(List<Card> cards) {
		super();
		this.id = UUID.randomUUID();
		this.cards = cards;
		Preconditions.checkArgument(checkSorted());
	}

	public void addCard(Card card) {
		if (!canAddCard()) {
			throw new IllegalStateException();
		}
		if (getHighestNumber() >= card.getNumber()) {
			throw new IllegalArgumentException();
		}
		cards.add(card);
	}

	public boolean canAddCard() {
		return cards.size() + 1 < Constants.MAX_SLOT_SIZE;
	}

	private boolean checkSorted() {
		for (int i = 0; i < cards.size() - 1; i++) {
			if (cards.get(i).getNumber() >= cards.get(i + 1).getNumber()) {
				return false;
			}
		}
		return true;
	}

	public List<Card> getCards() {
		return Collections.unmodifiableList(cards);
	}

	public int getNumberOfAllHorns() {
		return cards.stream().map(Card::getHorns).reduce(0, (x, y) -> x + y);
	}

	public int getLowestNumber() {
		return cards.get(0).getNumber();
	}

	public int getHighestNumber() {
		return cards.get(cards.size() - 1).getNumber();
	}

	public UUID getId() {
		return id;
	}

	@Override
	public String toString() {
		return cards.stream().map(Card::toString).reduce("",(s1,s2) -> s1+","+s2);
	}

}
