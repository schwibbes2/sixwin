package de.basti.p.sixwin.viewer;

import java.util.Collections;
import java.util.List;

import com.google.common.collect.Lists;

import de.basti.p.sixwin.Board;

public class BoardViewer {

	private final Board board;

	public BoardViewer(Board board) {
		super();
		this.board = board;
	}

	public List<SlotViewer> getSlots() {
		return Collections.unmodifiableList(Lists.transform(board.getSlots(), SlotViewer::new));
	}

}
