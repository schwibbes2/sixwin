package de.basti.p.sixwin.viewer;

import java.util.List;
import java.util.UUID;

import de.basti.p.sixwin.Card;
import de.basti.p.sixwin.Slot;

public class SlotViewer {

	private Slot slot;

	public SlotViewer(Slot slot) {
		super();
		this.slot = slot;
	}

	public List<Card> getCards() {
		return this.slot.getCards();
	}

	public int getLowestNumber() {
		return this.slot.getLowestNumber();
	}

	public int getHighestNumber() {
		return this.slot.getHighestNumber();
	}

	public UUID getID() {
		return slot.getId();
	}

	public int getNumberOfAllHorns() {
		return slot.getNumberOfAllHorns();
	}
}
