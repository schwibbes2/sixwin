package de.basti.p.sixwin.strategy;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import de.basti.p.sixwin.Card;
import de.basti.p.sixwin.IPlayerStrategy;
import de.basti.p.sixwin.Slot;
import de.basti.p.sixwin.viewer.BoardViewer;
import de.basti.p.sixwin.viewer.SlotViewer;

public class SimpleStrategy implements IPlayerStrategy {

	@Override
	public void reset() {
	}

	@Override
	public String getName() {
		return "SimpleStrategy";
	}

	@Override
	public Card chooseCard(BoardViewer boardViewer, Set<Card> possibleCards) {
		Card currentCard = null;
		int currentWeigth = 0;
		for (Card card : possibleCards) {
			Optional<SlotViewer> slot = getSlotToAppend(boardViewer, card);
			if (slot.isPresent()) {
				if (currentCard == null || slot.get().getCards().size() < currentWeigth) {
					currentCard = card;
					currentWeigth = slot.get().getCards().size();
				}
			} else {
				if (currentCard == null || 4 < currentWeigth) {
					currentCard = card;
					currentWeigth = 4;
				}
			}
		}
		return currentCard;
	}

	private Optional<SlotViewer> getSlotToAppend(BoardViewer boardViewer, Card card) {
		return boardViewer.getSlots().stream().filter(slot -> slot.getHighestNumber() < card.getNumber())
				.sorted((s1, s2) -> -s1.getHighestNumber() + s2.getHighestNumber()).findFirst();
	}

	@Override
	public void cardsPlayed(List<Card> cards) {
	}

	@Override
	public SlotViewer chooseSlot(BoardViewer boardViewer, Collection<Card> choosedCardsOfAllPlayers,
			Set<Card> remaingCards) {
		return boardViewer.getSlots().stream().sorted((s1, s2) -> s1.getNumberOfAllHorns() - s2.getNumberOfAllHorns())
				.findFirst().get();
	}

}
