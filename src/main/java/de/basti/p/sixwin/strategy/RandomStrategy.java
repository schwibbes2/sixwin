package de.basti.p.sixwin.strategy;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import de.basti.p.sixwin.Card;
import de.basti.p.sixwin.IPlayerStrategy;
import de.basti.p.sixwin.viewer.BoardViewer;
import de.basti.p.sixwin.viewer.SlotViewer;

public class RandomStrategy implements IPlayerStrategy {

	private static int c = 1;

	private int cc;

	public RandomStrategy() {
		cc = c++;
	}

	@Override
	public Card chooseCard(BoardViewer boardViewer, Set<Card> possibleCards) {
		return possibleCards.iterator().next();
	}

	@Override
	public void cardsPlayed(List<Card> cards) {
	}

	@Override
	public SlotViewer chooseSlot(BoardViewer boardViewer, Collection<Card> choosedCardsOfAllPlayers,
			Set<Card> remaingCards) {
		return boardViewer.getSlots().get((int) (Math.random() * boardViewer.getSlots().size()));
	}

	@Override
	public String getName() {
		return "Random-" + cc;
	}

	@Override
	public void reset() {
	}

}
