package de.basti.p.sixwin;

public class Card implements Comparable<Card> {

	private int number;
	private int horns;

	protected Card(int number, int horns) {
		super();
		this.number = number;
		this.horns = horns;
	}

	public int getHorns() {
		return horns;
	}

	public int getNumber() {
		return number;
	}

	@Override
	public int compareTo(Card o) {
		return number - o.number;
	}

	@Override
	public String toString() {
		return "(" + number + "," + horns + ")";
	}
}
