package de.basti.p.sixwin;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.google.common.base.Preconditions;
import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import de.basti.p.sixwin.viewer.BoardViewer;
import de.basti.p.sixwin.viewer.SlotViewer;

public class GameManager {

	private List<Player> players;
	private Board board;
	private boolean print;

	public GameManager(List<? extends IPlayerStrategy> players, boolean print) {
		super();
		this.print = print;
		Preconditions.checkArgument(players.size() == 4, "We need exact 4 players.");
		this.board = new Board(Constants.NUMBER_OF_SLOTS, Constants.NUMBER_OF_CARDS);
		this.players = players.stream().map(this::createPlayer).collect(Collectors.toList());
	}

	private Player createPlayer(IPlayerStrategy strategy) {
		Set<Card> cards = Sets.newHashSet();
		for (int i = 0; i < Constants.NUMBER_OF_CARDS_PER_PLAYER; i++) {
			cards.add(this.board.getAndRemoveRandomCardFromDeck());
		}
		return new Player(cards, strategy);
	}

	public GameResult runGame() {
		for (int round = 0; round < Constants.NUMBER_OF_CARDS_PER_PLAYER; round++) {
			printRoundNumber(round);
			printBoard();
			BiMap<Player, Card> playerToCardMap = HashBiMap.create();
			for (Player player : players) {
				Card choosedCard = player.getStrategy().chooseCard(new BoardViewer(board),
						Collections.unmodifiableSet(player.getCards()));
				playerToCardMap.put(player, choosedCard);
			}
			List<Card> cards = Lists.newArrayList(playerToCardMap.values());
			Collections.sort(cards);
			printPlayedCards(cards);
			for (Card card : cards) {
				Player player = playerToCardMap.inverse().get(card);
				printCurrentCard(player, card);
				player.getCards().remove(card);
				if (!canAppend(card)) {
					SlotViewer slotToReplaceViewer = player.getStrategy().chooseSlot(new BoardViewer(board),
							Collections.unmodifiableCollection(playerToCardMap.values()),
							Collections.unmodifiableSet(player.getCards()));
					Slot slotToReplace = board.getSlotWithID(slotToReplaceViewer.getID());
					int points = slotToReplace.getNumberOfAllHorns();
					player.addPoint(points);
					printPlayerGetsPoints(player, points);
					board.replaceSlot(slotToReplace, card);
				} else {
					Slot slotToAppend = getSlotToAppend(card);
					if (slotToAppend.canAddCard()) {
						slotToAppend.addCard(card);
					} else {
						int points = slotToAppend.getNumberOfAllHorns();
						player.addPoint(points);
						printPlayerGetsPoints(player, points);
						board.replaceSlot(slotToAppend, card);
					}
				}
				printBoard();
			}
			players.stream().map(Player::getStrategy)
					.forEach(strategy -> strategy.cardsPlayed(Collections.unmodifiableList(cards)));
		}
		for (Player player : players) {
			Preconditions.checkArgument(player.getCards().isEmpty());
		}
		return new GameResult(players.stream().collect(Collectors.toMap(Player::getStrategy, Player::getPoints)));
	}

	protected void printPlayerGetsPoints(Player player, int points) {
		if (print) {
			System.out.println("Player " + player.getStrategy().getName() + " get " + points + " Points");
		}
	}

	private Slot getSlotToAppend(Card card) {
		return board.getSlots().stream().filter(slot -> slot.getHighestNumber() < card.getNumber())
				.sorted((s1, s2) -> -s1.getHighestNumber() + s2.getHighestNumber()).findFirst().get();
	}

	private boolean canAppend(Card card) {
		return board.getSlots().stream().anyMatch(slot -> slot.getHighestNumber() < card.getNumber());
	}

	protected void printCurrentCard(Player p, Card card) {
		if (print) {
			System.out.println("Player " + p.getStrategy().getName() + " playes card: " + card.toString());
		}
	}

	protected void printPlayedCards(List<Card> cards) {
		if (print) {
			System.out.println("Played cards: " + cards.toString());
		}
	}

	protected void printRoundNumber(int round) {
		if (print) {
			System.out.println("Start round: " + (round + 1));
		}
	}

	protected void printBoard() {
		if (print) {
			System.out.println("Board: ");
			System.out.println(board.toString());
		}
	}
}
